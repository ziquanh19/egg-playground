FROM node:10.19

WORKDIR /app
COPY . /app
RUN apt-get update && apt-get -y install netcat && apt-get clean
RUN yarn install

EXPOSE 7777