#!/bin/bash

set -e
result=1
DB_URL=$1
DB_PORT=$2

wait_for()
{
  while [ $result -eq 1 ]
  do
    nc -z $DB_URL $DB_PORT
    result=$? 
    sleep 5
    echo "waiting Database wake up"
  done
  return 0
}

until wait_for == 0 
do
  echo "unknown error"
  sleep 10
done

echo "Database is up - executing command"
yarn run startf
exec "$@"