'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/fbsdk/ads', controller.fbsdk.ads);
  router.get('/fb/ads', controller.fb.ads);

  router.get('/account/campaignv4', controller.account.getCampaignsv4);
  router.get('/account/campaignv6', controller.account.getCampaignsv6);

  router.get('/account/adsetv4', controller.account.getAdsetsv4);
  router.get('/account/adsetv6', controller.account.getAdsetsv6);

  router.get('/account/adsv4', controller.account.getAdsv4);
  router.get('/account/adsv6', controller.account.getAdsv6);

  router.get('/getpic', controller.webshot.index);


  router.get('/fbsdk', controller.fbbusinesssdk.main.index);


  router.get('/reducelimit', controller.limit.reduce.main);
  router.get('/queue/newWork', controller.bullqueue.index);

};
