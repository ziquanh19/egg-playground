'use strict'

require('dotenv').config()

const Service = require('egg').Service

const path = require('path')
const { promisify } = require('util')
const puppeteer = require('puppeteer');
//const webshotAsync = promisify(puppeteer)
var fs = require('fs');


class ShotPuppy extends Service {


    async _takePhoto ( url, id) {

        const { ctx } = this;

        let filename = `${id}.png`
        const destination = path.resolve( __dirname, `../public/tmp/${filename}`)

        console.log("destination")
        console.log(destination)

        const emulateOptions = {
          viewport: {
            width: 1024,
            height: 1500
          },
          userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
        }

        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.emulate(emulateOptions)
        await page.goto(url);
        let contentHeight = await page.evaluate(() => {
          return document.getElementsByTagName('html')[0].offsetHeight;
        })
        console.log( contentHeight)
        await page.screenshot({
          path: destination,
          type: 'png',
          fullpage: true,
          clip: {
            x: 0,
            y: 0,
            width: 502,
            height: contentHeight
          }
        });
      
        await browser.close();

    }

}

module.exports = ShotPuppy