'use strict';

require('dotenv').config();

const Service = require('egg').Service;
const adsSdk = require('facebook-nodejs-business-sdk');

class Main extends Service {
  async fetch() {
    // const accessToken = this._account.access_token;
    // const accountId = `act_${this._account.account_id}`;
    const accessToken = process.env.ACCESS_TOKEN;
    const accountId = `act_${process.env.ACCOUNT_ID_KiiwiO}`;

    const api = adsSdk.FacebookAdsApi.init(accessToken);
    api.setDebug(true);

    // if (this._query.hasOwnProperty('undefined')) {
    //   return { datas: [], after: '' };
    // }

    // const { fields, ...params } = this._query;
    // const fields = 'clicks,cpc,cpm,ctr,purchase_roas,website_purchase_roas,mobile_app_purchase_roas';
    // const params = {
    //   time_range: '{"since":"2020-04-25","until":"2020-10-22"}',
    //   time_increment: '1',
    //   level: 'account',
    //   filtering: '',
    //   fields: 'clicks,cpc,cpm,ctr,purchase_roas,website_purchase_roas,mobile_app_purchase_roas',
    //   access_token: 'EAAC9E8WVmVIBAE1Fn74cIVkWfMmD9XTheBpMMBpZBNsh0q7Vd16ujDNYB0MyPmzQZBPb0dOPoZA28g55kmLPeeNY4wZCmgZCzwOUPWSbZCNSy0w9qVznCZC3USupbNu3fHFZCZBBhCyNbbm6OZAbBqsWuZA8xKqb1oD2ZBu6SF3ehmmsNwJJ3zU34Jj5',
    //   limit: 2000,
    // };

    const fields = 'clicks,cpc,cpm,ctr,purchase_roas,website_purchase_roas,mobile_app_purchase_roas';
    const params = {
      time_range: '{"since":"2020-04-25","until":"2020-10-15"}',
      time_increment: '1',
      level: 'adset',
      filtering: '',
      fields: 'clicks,cpc,cpm,ctr,purchase_roas,website_purchase_roas,mobile_app_purchase_roas',
      access_token: 'EAAC9E8WVmVIBAE1Fn74cIVkWfMmD9XTheBpMMBpZBNsh0q7Vd16ujDNYB0MyPmzQZBPb0dOPoZA28g55kmLPeeNY4wZCmgZCzwOUPWSbZCNSy0w9qVznCZC3USupbNu3fHFZCZBBhCyNbbm6OZAbBqsWuZA8xKqb1oD2ZBu6SF3ehmmsNwJJ3zU34Jj5',
      limit: 1,
    };


    const AdAccount = adsSdk.AdAccount;
    const account = new AdAccount(accountId);
    const insights = await account.getInsights(fields.split(','), params);

    return { datas: insights, after: insights.paging ? insights.paging.cursors.after : '' };
  }

}

module.exports = Main;
