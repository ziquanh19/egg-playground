
'use strict';

require('dotenv').config();

const Service = require('egg').Service;


class Account extends Service {
  /**
   *
   * @param {String} hashid
   * @param {String} query
   * @return {Object}
   */
  async getInsights(account, hashid, query) {
    const [ id ] = this.app.hashids.decode(hashid);
    const account = await this.ctx.model.Account.findByPk(id);
    if (!account) this.ctx.throw(404, 'account not found');

    const fetcher = new InsightsFetcher(this.ctx);

    const insights = await fetcher
      .setAccount(account)
      .setQuery(query)
      .fetch();

    return { results: insights.datas, cursors: { after: insights.after } };
  }

}

module.exports = Account;
