'use strict'

require('dotenv').config()
const Service = require('egg').Service
const FQB = require('fqb')
const union = require('lodash/union')

class CampaignService extends Service {

    async list () {
        if (1 === 1 ) return await this._listFacebook()
        //return await Promise.resolve({})
    }

    /**
     * List facebook insights.
     *
     * @param {Object} 
     * @return {Promise}
     */
    async _listFacebook () {
        const data = await this._fetchFacebookData()
        console.log( "_fetchFacebookData data ")
        console.log(data)
        return this._proccessFacebbokData(data)
    }


    /**
     * Fetch facebook insights.
     *
     * @param {Object} query
     * @return {Promise}
     */
    _fetchFacebookData () {
        /*
        const modifiers = {}
        if (query.filterings && query.filterings.length !== 0) modifiers.filtering = query.filterings
        const fields = query.fields || []
        */
        console.log("_fetchFacebookData");

        const url = new FQB()
        .node(`act_${process.env.ACCOUNT_ID_KiiwiO}/campaigns`)
        .graphVersion(process.env.FACEBOOK_GRAPH_VERSION6)
        .accessToken(process.env.ACCESS_TOKEN)
        .limit(9999)
        .asUrl()

        console.log("url");
        console.log(url);

        return this.ctx.curl(url, { dataType: 'json', timeout: 60000 })
    }



    /**
     * Proccess facebook data
     *
     * @param {Object} originData
     * @return {Object}
     */
    _proccessFacebbokData (originData) {
        if (!originData.data.data) return originData.data
        const campaigns = originData.data.data
        .reduce((normalizedData, campaign) => Object.assign({}, normalizedData, { [campaign.id]: campaign }), {})
        
        //console.log("_proccessFacebbokData originData");
        //console.log(originData.data);
        //console.log("_proccessFacebbokData campaigns");
        //console.log(campaigns);
        
        
        return { originData: originData.data, campaigns, RequestUrl: originData.res.requestUrls }
    }

}

module.exports = CampaignService
