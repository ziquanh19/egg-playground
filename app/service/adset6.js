'use strict'

require('dotenv').config()

const Service = require('egg').Service
const FQB = require('fqb')
const union = require('lodash/union')
const ShapeJson = require('@ballfish/shape_json')


class Adset extends Service {

    /**
     * List insights.
     *
     * @param {Object} query
     * @return {Promise}
     */
    async list () {
        if (1 === 1) return await this._listFacebook()
        //return await Promise.resolve({})
    }

    /**
     * List facebook insights.
     *
     * @param {Object} query
     * @return {Promise}
     */
    async _listFacebook () {
        const data = await this._fetchFacebookData()
        return this._proccessFacebbokData(data)
    }

    _fetchFacebookData () {
        const fields = []
        const campaignEdge = new FQB()
            .edge('campaign')
            .fields(['id'])

        const url = new FQB()
            .node(`act_${process.env.ACCOUNT_ID_KiiwiO}/adsets`)
            .graphVersion(process.env.FACEBOOK_GRAPH_VERSION6)
            .accessToken(process.env.ACCESS_TOKEN)
            .limit(9999)
            .fields(union(fields.concat(['id', 'name', 'status', campaignEdge])))
            .asUrl()

        return this.ctx.app.curl(url, { dataType: 'json', timeout: 60000 })
    }

    _proccessFacebbokData (originData) {
        const schema = {
          id: {},
          name: {},
          status: {},
          campaign: { to: 'campaignId', customParse: (value) => value.id }
        }
        const option = { error: 'ignore', otherProperties: 'keep' }
        if (!originData.data.data) return originData.data
        const adsets = originData.data.data
          .map(adset => Object.assign({}, ShapeJson.shape(adset, schema, option)))
          .reduce((normalizedData, adset) => Object.assign({}, normalizedData, { [adset.id]: adset }), {})
        return { originData: originData.data, adsets, RequestUrl: originData.res.requestUrls }
    }



}

module.exports = Adset