'use strict'

require('dotenv').config()

const Service = require('egg').Service
const FQB = require('fqb')
const union = require('lodash/union')
const ShapeJson = require('@ballfish/shape_json')


class Ads extends Service {

    /**
     * List insights.
     *
     * @param {Object} query
     * @return {Promise}
     */
    async list () {
        if (1 === 1) return await this._listFacebook()
        //return await Promise.resolve({})
    }

    /**
     * List facebook insights.
     *
     * @param {Object} query
     * @return {Promise}
     */
    async _listFacebook () {
        const data = await this._fetchFacebookData()
        return this._proccessFacebbokData(data)
    }

  /**
   * Fetch facebook insights.
   *
   * @param {Object} query
   * @return {Promise}
   */
    async _fetchFacebookData () {
        const previewsEdge = new FQB()
        .edge('previews')
        .limit(9999)
        .modifiers({
            ad_format: 'DESKTOP_FEED_STANDARD',
            locale: 'zh_TW'
        })

        const adcreativesEdge = new FQB()
        .edge('adcreatives')
        .limit(9999)
        .fields(['thumbnail_url'])
        .modifiers({
            ad_format: 'DESKTOP_FEED_STANDARD'
        })

        const campaignEdge = new FQB()
        .edge('campaign')
        .fields(['id'])

        const adsetEdge = new FQB()
        .edge('adset')
        .fields(['id'])

        const edgeArray = [previewsEdge, adcreativesEdge, campaignEdge, adsetEdge]

        const fields = []

        const url = new FQB()
        .node(`act_${process.env.ACCOUNT_ID_KiiwiO}/ads`)
        .graphVersion(process.env.FACEBOOK_GRAPH_VERSION6)
        .accessToken(process.env.ACCESS_TOKEN)
        .fields(union(fields.concat(['id', 'name', 'status', ...edgeArray, ...fields])))
        .asUrl()

        const res = await this.ctx.app.curl(url, { dataType: 'json', timeout: 60000 })
        if (!res.data) throw res
        if (!res.data.data) throw res.data
        const getNext = async (data, pack, requestUrls) => {
        if (data && data.paging && data.paging.next) {
            const nextData = await this.ctx.app.curl(data.paging.next, { dataType: 'json', timeout: 60000 })
            if (!nextData.data) throw nextData

            // console.log("pack");
            // console.log(pack);
            const nextRequestUrls = [].concat(requestUrls).concat(nextData.res.requestUrls)
            // console.log(nextRequestUrls)
            const newPack = [].concat(pack).concat(nextData.data.data)
            return await getNext(nextData.data, newPack, nextRequestUrls)
        }

        return { pack: pack, requestUrls: requestUrls }
        }
        // console.log("res.data")
        // console.log(res.data)
        // console.log("res.data.data")
        // console.log(res.data.data)

        const packages = await getNext(res.data, res.data.data, res.res.requestUrls)
        return { 
                    data: 
                    { 
                        data: packages.pack,
                        requestUrls: packages.requestUrls
                    } 
                }
    }

    /**
     * Proccess facebook data
     *
     * @param {Object} originData
     * @return {Object}
     */
    _proccessFacebbokData (originData) {
        const schema = {
        id: {},
        name: {},
        status: {},
        campaign: { to: 'campaignId', customParse: (value) => value.id },
        adset: { to: 'adsetId', customParse: (value) => value.id }
        }
        const option = { notFoundProperties: 'give-default', otherProperties: 'keep' }
        if (!originData.data.data) return originData.data

        const ads = originData.data.data
        .reduce((normalizedData, ad) => {
            const newAd = ShapeJson.shape(ad, schema, option)
            delete newAd.adcreatives
            if (ad.adcreatives && ad.adcreatives.data && ad.adcreatives.data[0] && ad.adcreatives.data[0].thumbnail_url) {
            newAd.hasImage = true
            newAd.image = ad.adcreatives.data[0].thumbnail_url
            } else {
            newAd.hasImage = false
            }
            if (ad.previews && ad.previews.data && ad.previews.data[0] && ad.previews.data[0].body) newAd.previews = ad.previews.data[0].body
            return { ...normalizedData, [ad.id]: newAd }
        }, {})
        return { originData: originData.data, ads, requestUrls: originData.requestUrls }
    }



}

module.exports = Ads