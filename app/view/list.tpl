<html>
  <head>
    <title>fbapi</title>
    <link rel="stylesheet" href="" />
    <style type="text/css">
      body {
        word-wrap: break-word
      }
    </style>  
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId            : '920515901337067',
          autoLogAppEvents : true,
          xfbml            : true,
          version          : 'v6.0'
        });
      };
    </script>

    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/main.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    
  </head>
  <body>

    <div class="container-fluid" style="background-color: rgb(177, 173, 173);">
      <div class="row">
        <div class="col-1">
          <p> SDK </p>
        </div>  
        <div class="col-1">
          <button onclick="getFbSdk()"> 欄位名稱 </button>
          <p id="getFbSdk" ></p>
        </div>
      </div>

      <div class="row">

        <div class="col-2">
          <p > FQB <br> 豐禾開發(KiiWio)  <br> GRAPH API version 4.0  </p>
        </div>
        <div class="col-1">
          <button onclick="getKiiwioCampaignV4()"> campaign4 </button>
          <p id="getKiiwioCampaignV4" ></p>
        </div>
        <div class="col-1">
          <button onclick="getKiiwioAdSetV4()"> adset4 </button>
          <p id="getKiiwioAdSetV4" ></p>
        </div>
        <div class="col-2">
          <button onclick="getKiiwioAdsV4()"> ads4 </button>
          <p id="getKiiwioAdsV4" ></p>
        </div>

      </div>

      <div class="row">

        <div class="col-2">
          <p > FQB <br> 豐禾開發(KiiWio)  <br> GRAPH API version 6.0  </p>
        </div>
        <div class="col-1">
          <button onclick="getKiiwioCampaignV6()"> campaign6 </button>
          <p id="getKiiwioCampaignV6" ></p>
        </div>
        <div class="col-1">
          <button onclick="getKiiwioAdSetV6()"> adset6 </button>
          <p id="getKiiwioAdSetV6" ></p>
        </div>
        <div class="col-2">
          <button onclick="getKiiwioAdsV6()"> ads6 </button>
          <p id="getKiiwioAdsV6" ></p>
        </div>

      </div>

    </div>

  </body>


  <script>
    function getFbSdk () {
      $.ajax({
          type: "GET", //傳送方式
          url: "/fbsdk/ads", //傳送目的地
          success: function(data) {
              console.log("欄位名稱")
              console.log(data)
              $("#getFbSdk").text("顯示於console");
          },
          error: function(jqXHR) {
              $("#getFbSdk").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
          }
      })    
    }

    function getKiiwioCampaignV4 () {
      $.ajax({
          type: "GET", //傳送方式
          url: "/account/campaignv4", //傳送目的地
          success: function(data) {
              console.log(" campaign  4.0")
              console.log(data);
              $("#getKiiwioCampaignV4").text("顯示於console campaign  4.0 \n ");
              $("#getKiiwioCampaignV4").append("<a href= " + data.RequestUrl + "> 資料連結 </a>")
          },
          error: function(jqXHR) {
              $("#getKiiwioCampaignV4").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
          }
      })     
    }
    
    function getKiiwioAdSetV4 () {
      $.ajax({
          type: "GET", //傳送方式
          url: "/account/adsetv4", //傳送目的地
          success: function(data) {
              console.log(" AdSet  4.0")
              console.log(data);
              $("#getKiiwioAdSetV4").text("顯示於console AdSet  4.0 \n ");
              $("#getKiiwioAdSetV4").append("<a href= " + data.RequestUrl + "> 資料連結 </a>");
          },
          error: function(jqXHR) {
              $("#getKiiwioAdSetV4").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
          }
      })     
    }

    function getKiiwioAdsV4 () {
      $.ajax({
        type: "GET",
        url: "/account/adsv4",
        success: function(data) {
            console.log(" Ads  4.0")
            console.log(data);
            $("#getKiiwioAdsV4").text("顯示於console Ads  4.0 \n ");
            console.log(data.originData.requestUrls);
            for ( i=0 ; i < data.originData.requestUrls.length; i++ ) {
              $("#getKiiwioAdsV4").append("<a href= " + data.originData.requestUrls[i] + "> 資料連結 "+ i + " \n </a>");
            }
        },
        error: function(jqXHR) {
            $("#getKiiwioAdsV4").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
        }  
      })
    }

    function getKiiwioCampaignV6 () {
      $.ajax({
          type: "GET", //傳送方式
          url: "/account/campaignv6", //傳送目的地
          success: function(data) {
              console.log(" campaign  6.0")
              console.log(data);
              $("#getKiiwioCampaignV6").text("顯示於console campaign  6.0 \n ");
              $("#getKiiwioCampaignV6").append("<a href= " + data.RequestUrl + "> 資料連結 </a>")
          },
          error: function(jqXHR) {
              $("#getKiiwioCampaignV6").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
          }
      })     
    }

    function getKiiwioAdSetV6 () {
      $.ajax({
          type: "GET", //傳送方式
          url: "/account/adsetv6", //傳送目的地
          success: function(data) {
              console.log(" AdSet  6.0")
              console.log(data);
              $("#getKiiwioAdSetV6").text("顯示於console AdSet  6.0 \n ");
              $("#getKiiwioAdSetV6").append("<a href= " + data.RequestUrl + "> 資料連結 </a>")
          },
          error: function(jqXHR) {
              $("#getKiiwioAdSetV6").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
          }
      })     
    }

    function getKiiwioAdsV6 () {
      $.ajax({
        type: "GET",
        url: "/account/adsv6",
        success: function(data) {
            console.log(" Ads  6.0")
            console.log(data);
            $("#getKiiwioAdsV6").text("顯示於console Ads  6.0 \n ");
            console.log(data.originData.requestUrls);
            for ( i=0 ; i < data.originData.requestUrls.length; i++ ) {
              $("#getKiiwioAdsV6").append("<a href= " + data.originData.requestUrls[i] + "> 資料連結 "+ i + " \n </a>");
            }
        },
        error: function(jqXHR) {
            $("#getKiiwioAdsV6").html('<font color="#ff0000">發生錯誤：' + jqXHR.status + '</font>');
        }  
      })
    }





  </script>








</html>