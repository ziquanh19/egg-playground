'use strict';

const adsSdk = require('facebook-nodejs-business-sdk');

require('dotenv').config();

const Controller = require('egg').Controller;

class FbBusinessSdkController extends Controller {
  async index() {
    const { ctx } = this;
    const api = adsSdk.FacebookAdsApi.init(process.env.ACCESS_TOKEN);
    const AdAccount = adsSdk.AdAccount;
    const Campaign = adsSdk.Campaign;
    const account = new AdAccount('act_608638929560907');
    console.log(account.id); // fields can be accessed as properties

    // console.log('AdAccount.Fields.name');
    // console.log(AdAccount.Fields.name);
    // console.log('AdAccount.Fields.age');
    // console.log(AdAccount.Fields.age);


    // console.log(account);

    // account
    //   .read([ AdAccount.Fields.name, AdAccount.Fields.age ])
    //   .then(account => {
    //     console.log(account);
    //   })
    //   .catch(error => {
    //   });


    account.getCampaigns([ Campaign.Fields.name ], { limit: 2 })
      .then(campaigns => {
        if (campaigns.length >= 2 && campaigns.hasNext()) {
          return campaigns.next();
        }
        Promise.reject(
          new Error('campaigns length < 2 or not enough campaigns')
        );

      })
      .then(campaigns => {
        if (campaigns.hasNext() && campaigns.hasPrevious()) {
          return campaigns.previous();
        }
        Promise.reject(
          new Error('previous or next is not true')
        );

        return campaigns.previous();
      })
      .catch(error => {
      });


    const result = await ctx.service.fbbusinesssdk.main.fetch();
    ctx.body = result;


    // console.log('fifi nodejs-buisiness-sdk');


  }
}

module.exports = FbBusinessSdkController;
