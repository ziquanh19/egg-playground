'use strict';

require('dotenv').config();

const Controller = require('egg').Controller;
const FQB = require('fqb');


class AccountController extends Controller {
  async getCampaignsv4() {
    const ctx = this.ctx;
    const data = await ctx.service.campaign4.list();
    console.log('ctx.body');
    // console.log(data)
    ctx.status = 200;
    ctx.body = data;
  }

  async getCampaignsv6() {
    const ctx = this.ctx;
    const data = await ctx.service.campaign6.list();
    console.log('ctx.body');
    // console.log(data)
    ctx.status = 200;
    ctx.body = data;
  }

  /**
     * GET /v4/accounts/:accountId/adsets
     */
  async getAdsetsv4() {
    const ctx = this.ctx;
    const data = await ctx.service.adset4.list();
    ctx.status = 200;
    ctx.body = data;
  }

  async getAdsetsv6() {
    const ctx = this.ctx;
    const data = await ctx.service.adset6.list();
    ctx.status = 200;
    ctx.body = data;
  }

  /**
     * GET /v4/accounts/:accountId/ads
     */
  async getAdsv4() {
    const ctx = this.ctx;
    const data = await ctx.service.ads4.list();
    ctx.status = 200;
    ctx.body = data;
  }
  async getAdsv6() {
    const ctx = this.ctx;
    const data = await ctx.service.ads6.list();
    ctx.status = 200;
    ctx.body = data;
  }


}

module.exports = AccountController;
