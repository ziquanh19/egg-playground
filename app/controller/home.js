'use strict';

require('dotenv').config();

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    const data = {
      env: {
        POSTGRES_URL: process.env.POSTGRES_URL,
        POSTGRES_PORT: process.env.POSTGRES_PORT,
        WEB_URL: process.env.WEB_URL,
        WEB_API: process.env.WEB_API,
        WEBAPP_NAME: process.env.WEBAPP_NAME,
        STATUS: process.env.STATUS,
      },
    };

    await ctx.render('list.tpl', data);
  }
}

module.exports = HomeController;
