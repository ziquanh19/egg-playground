'use strict';

require('dotenv').config();

const Controller = require('egg').Controller;

const path = require('path');
const { promisify } = require('util');
const webshot = require('webshot');
const webshotAsync = promisify(webshot);

class Webshot extends Controller {

  async index() {
    const { ctx } = this;

    const url = 'https://www.facebook.com/ads/api/preview_iframe.php?d=AQKe7qbOrawXHN-TZ9YmTRYhKlbkTCBLBY07dDSfBP1fBMWksb-bU9cT5jDipNF__HMwznfkDQYSrRLUGK9k4vqpLOf_AJSctqvdUjSfJfbZhRQfvq20jI_ha5RVeFvNBsPjSl1KwHM4oLG9z5C_StFmPbWjwBWpsXfA4G9stbreSWO-Ms2fYoI1T7uOROyEe2kz27tYzqchHSH_YNO_KTupHLSz6Gc8EmI-QZuWrrY7dl4lGKDc5s6OOynxuM0RahLbnvH9DKcPOcTPkj_voS2QruHvCIzLmsgZPeVSnOKpoQ5PyNeQexzY_tFjf-o452ucEIFp2WmKfi_-O1pz8UMe&t=AQLWXh8Lw3-lu-31&locale=zh_TW';

    const canUrl = 'https://www.facebook.com/ads/api/preview_iframe.php?d=AQIwOSKrIJnE3qmMHldH-tXdar8I3jAeQ2ofFJVvyPA9JpuEh0_20CRMyDwrr-_Opb29bSAWctFmPMiHnFTcOcM75fnZayNfk5tQpS64hiEpn20TtN-ocNB0-oKOvZFjywwK-6UwqkvFt3dlqrNQ4SSPwFePa-C9XRq4dB-Zn8yAvcjO8XWeB5jY9aUnIIw8hhxgQta6TPUrvPB0eFf9jT5kt7bFEv-ZigLesPgrubesu52ceQbrbGbRGIyTOhAb2vOuO5R4k_umG18C4luAXNlM7Y2qbOUMOzDb2ia2VyEJl3LpH7WSmCwYsM_DkLESWJT6dvpSS-Exl9Ao7rJLdM4B&t=AQKVuVh4PZUMAu-3&locale=zh_TW';
    const id = 0;

    const res2 = await ctx.service.shotpuppy._takePhoto(canUrl, id);
    // const res = await ctx.service.shot._takePhoto( canUrl, id);
    // console.log (res)
  }


}

module.exports = Webshot;
