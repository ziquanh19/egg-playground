'use strict';

require('dotenv').config();

const { newWork } = require('../util/bull');

const Controller = require('egg').Controller;

class Bullqueue extends Controller {

  async index() {
    const { ctx } = this;
    for (let k = 0; k < 10; k++) {
      await newWork({ seq: k });
    }
    ctx.body = 'ya';
  }

}

module.exports = Bullqueue;
