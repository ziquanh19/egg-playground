'use strict';

const bizSdk = require('facebook-nodejs-business-sdk');

require('dotenv').config();

const Controller = require('egg').Controller;

class FbSdkController extends Controller {
  async ads() {
    const { ctx } = this;

    const accessToken = process.env.ACCESS_TOKEN;
    const accountId = process.env.ACCOUNT_ID_KiiwiO;

    const FacebookAdsApi = bizSdk.FacebookAdsApi.init(accessToken);
    const AdAccount = bizSdk.AdAccount;
    const Campaign = bizSdk.Campaign;

    const account = new AdAccount('act_' + accountId);
    let campaigns;

    await account.read([ AdAccount.Fields.name ])
      .then(account => {
        return account.getCampaigns([ Campaign.Fields.name ], { limit: 10 }); // fields array and params
      })
      .then(result => {
        campaigns = result;
        console.dir('一個廣告 ');
        console.dir(campaigns[0]);
        console.dir('資料欄位 ', campaigns[0]._fields);
        console.dir(campaigns[0]._fields);
        ctx.body = campaigns[0]._fields;
        campaigns.forEach(campaign => {
          console.log(campaign.name);
        });

      })
      .catch(console.error);


  }


}

module.exports = FbSdkController;
