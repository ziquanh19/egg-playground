'use strict';

require('dotenv').config();
const pLimit = require('p-limit');
const limit = pLimit(parseInt(10));


const Controller = require('egg').Controller;


class ReduceController extends Controller {

  async main() {
    const { ctx } = this;

    let hugeArray = [];
    let meetObject = [];


    for (let i = 0; i < 10000; i++) {
      hugeArray = hugeArray.concat(i);
    }

    for (let i = 0; i < 10000; i++) {
      meetObject = Object.assign({}, {
        hi: 'hi',
        seq: i,
      });
    }


    const result = await this._group(hugeArray);


    const result2 = await this._group2(hugeArray);
    const result3 = await this._group3(result2);

    ctx.status = 200;
    ctx.body = { result3, result, hugeArray, meetObject };
  }


  async _group(someThing) {
    return someThing
      .map(meta => ({ meta }))
      .reduce((object, item, index) => {
        // console.log('reduce' + index);
        // return Object.assign({}, object, { item });
        return limit(() => Object.assign({}, object, { item }));
      }, {});
  }

  async _group2(someThing) {
    return someThing
      .map(meta => ({ meta }));
    //   .reduce((object, item, index) => {
    //     console.log('reduce' + index);
    //     // return Object.assign({}, object, { item });
    //     return limit(() => Object.assign({}, object, { item }));
    //   }, {});
  }

  async _group3(someThing) {
    return someThing
    //   .map(meta => ({ meta }));
      .reduce((object, item, index) => {
        console.log('reduce' + index);
        // return Object.assign({}, object, { item });
        return limit(() => Object.assign({}, object, { item }));
      }, {});
  }


}

module.exports = ReduceController;
