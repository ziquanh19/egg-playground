'use strict';
require('dotenv').config();
const Queue = require('bull');
const moment = require('moment');

const videoQueue = new Queue('video transcoding', 'redis://127.0.0.1:6379');
const audioQueue = new Queue('audio transcoding', {
  redis: {
    port: 6379,
    host: '127.0.0.1',
    // password: 'foobared',
  },
}); // Specify Redis connection using object
const imageQueue = new Queue('image transcoding');

const messageQueue = new Queue('message transcoding', {
  redis: {
    port: 6379,
    host: '127.0.0.1',
    // password: 'foobared',
  },
  limiter: {
    max: 3,
    duration: 1000,
  },
});

messageQueue.process((job, done) => {
  console.dir('job.data');
  console.dir(job.data);
  //   console.dir('done');
  //   console.dir(done);
  done();
  return '111';
//   this.ctx.body = '111';
});

messageQueue.on('completed', (job, result) => {
  console.log(`The job has been done! ${job.data.seq}`);
});


function newWork(data) {
  messageQueue.add(data);
}

module.exports = {
  newWork,
};
